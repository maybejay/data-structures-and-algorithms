package com.maybejay.datastructuresalgorithms.datastructures.impl.queues;

import com.maybejay.datastructuresalgorithms.datastructures.interfaces.queue.Queue;
import com.maybejay.datastructuresalgorithms.utilities.exceptions.EmptyQueueException;
import com.maybejay.datastructuresalgorithms.utilities.nodes.QueueNode;

/**
 * Abstract implementation of a FIFO queue.
 * @param <E>
 */

public abstract class AbstractQueue<E> implements Queue<E> {

	protected QueueNode<E> head;
	protected QueueNode<E> tail;
	protected int capacity;

	protected int size;

	protected AbstractQueue(int capacity) {
		this.capacity = capacity;
		this.head = this.tail = null;
	}

	protected AbstractQueue(E initialData, int capacity) {
		this.capacity = capacity;
		this.head = this.tail = new QueueNode<E>(initialData);
	}

	/**
	 *	Enqueues data into the queue.
	 * @param data to be enqueued
	 * @return this
	 */
	public abstract AbstractQueue<E> enqueue(E data);

	/**
	 *	Dequeues the first element.
	 * @return data
	 */
	public abstract E dequeue() throws EmptyQueueException;

	/**
	 * 	Returns the count of the queue.
	 * @return size
	 */
	public int size() {
		return this.size;
	}

	/**
	 * Returns the queue as a form of T.
	 * @param t class to be converted into.
	 * @return new structure
	 * @param <T>
	 */
	public abstract  <T> T getQueueAs(Class<T> t);

	/**
	 * Returns but not removes the head element.
	 * @return head
	 * @throws EmptyQueueException if the queue is empty
	 */
	public E peek() throws EmptyQueueException {
		if (this.head == null) {
			throw new EmptyQueueException("Queue is empty. Peek aborted!");
		}
		return this.head.getData();
	}

	/**
	 * Returns but not removes the tail element.
	 * @return tail
	 * @throws EmptyQueueException if the queue is empty
	 */

	public E peekLast() throws EmptyQueueException {
		if (this.size == 0) {
			throw new EmptyQueueException("Queue is empty. Peek aborted!");
		}
		return this.tail.getData();
	}
}
