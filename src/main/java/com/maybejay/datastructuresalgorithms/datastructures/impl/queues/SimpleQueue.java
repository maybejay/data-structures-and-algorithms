package com.maybejay.datastructuresalgorithms.datastructures.impl.queues;

import com.maybejay.datastructuresalgorithms.utilities.exceptions.EmptyQueueException;
import com.maybejay.datastructuresalgorithms.utilities.nodes.QueueNode;

public class SimpleQueue<E> extends AbstractQueue<E> {

	public SimpleQueue() {
		super(-1);
	}

	public SimpleQueue(E initialData) {
		super(initialData, -1);
	}

	public SimpleQueue(int limitedCapacity) {
		super(limitedCapacity);
	}

	public SimpleQueue(E initialData, int limitedCapacity) {
		super(initialData, limitedCapacity);
	}

	/**
	 * Enqueues data into the queue.
	 *
	 * @param data to be enqueued
	 * @return this
	 */
	@Override
	public AbstractQueue<E> enqueue(E data) {
		if (data == null) {
			throw new IllegalArgumentException("Data cannot null");
		}
		QueueNode<E> newNode = new QueueNode<>(data, this.tail);
		this.tail.setNext(newNode);
		this.tail = newNode;
		if (this.head == null) {
			this.head = newNode;
		}
		this.size++;
		return this;
	}

	/**
	 * Dequeues the first element.
	 *
	 * @return data
	 */
	@Override
	public E dequeue() throws EmptyQueueException {
		if (this.size <= 0) {
			throw new EmptyQueueException("Queue is empty. Dequeue aborted!");
		}
		QueueNode<E> currNode = this.head;
		this.head = this.head.getNext();
		currNode.setNext(null);
		this.size--;
		if (this.size == 0) {
			this.head = null;
			this.tail = null;
		}
		return currNode.getData();
	}

	/**
	 * Returns the queue as a form of T.
	 *
	 * @param t   class to be converted into.
	 * @param <T>
	 * @return new structure
	 */
	@Override
	public <T> T getQueueAs(Class<T> t) {
		throw new UnsupportedOperationException("Not implemented yet");
	}
}
