package com.maybejay.datastructuresalgorithms.datastructures.interfaces.queue;

import com.maybejay.datastructuresalgorithms.utilities.exceptions.EmptyQueueException;

public interface Queue<E> {

	/**
	 *	Enqueues data into the queue.
	 * @param data to be enqueued
	 * @return this
	 */
	Queue<E> enqueue(E data);

	/**
	 *	Dequeues the first element.
	 * @return data
	 */
	E dequeue() throws EmptyQueueException;

	/**
	 * Returns but not removes the head element.
	 * @return head
	 * @throws EmptyQueueException if the queue is empty
	 */
	E peek() throws EmptyQueueException;

	/**
	 * Returns but not removes the tail element.
	 * @return tail
	 * @throws EmptyQueueException if the queue is empty
	 */
	E peekLast() throws EmptyQueueException;

	/**
	 * 	Returns the count of the queue.
	 * @return size
	 */
	int size();

	/**
	 * Returns the queue as a form of T.
	 * @param t class to be converted into.
	 * @return new structure
	 * @param <T>
	 */
	<T> T getQueueAs(Class<T> t);
}
